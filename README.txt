Price Detection from Price Tags
Adrian Cosma, IA

The price tag dataset can be found at this location (Google Drive):
https://drive.google.com/drive/folders/1mvLJJj0vf8TAx-AAzFBLOVa74dNGJeWD?usp=sharing

Check out Sprint 2's demo video at this link:
https://www.youtube.com/watch?v=IaBHsyEFpbc

The folder structure is organized as follows:

bibliography/
  - contains .PDF documents used as reference

code/
  - contains the code necessary for training / testing the various pipeline components

code/training/
  - currently contains the training code for the segmentation network
  - run python train.py to run the training (make sure to change the path to the annotated data locations)

code/workflow/ & code/components
  - contains the classes that wrap trained models / procedures for better integration futher in the pipeline


deliverables/
  sprint1/
  sprint2/
  sprint3/
  sprint4/

  - contains all the presentations and documents for each sprint

