import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import pandas as pd

import sys
sys.path.append('..')
import cv2
import numpy as np
import json
import base64
from PIL import Image
import requests
from io import BytesIO

import components
from workflow import Workflow
from utils import log

workflow = Workflow([
    components.PriceDetector(),
    components.BarcodeDetector(),
], verbose = False)

flow = Workflow([
    components.LabelDetector(),
    components.PerspectiveCorrectorEnhanced(),
    workflow,
], verbose = False)

data = pd.read_csv('mturk_test_split_fine.csv')

price_hits = 0
barcode_hits = 0
both_hits = 0

bad_barcodes = {
    'id': [],
    'true': [],
    'detected': [],
    'url': []
}
bad_prices = {
    'id': [],
    'true': [],
    'detected': [],
    'url': []
}

os.makedirs('mturk_cache/', exist_ok = True)

for i, row in data.iterrows():
    # if row['HITId'] != '3511RHPAEY8VAM6VN4HY8V8ZW6HLRV':
        # continue

    print(i, len(data.index))
    y_true = eval(row['Detections'])

    image_path = f'mturk_cache/{row["HITId"]}.png'
    if os.path.exists(image_path):
        img = cv2.imread(image_path)
    else:
        response = requests.get(row['Input.image_url'])
        img = cv2.cvtColor(np.array(Image.open(BytesIO(response.content))), cv2.COLOR_BGR2RGB)
        print("Saved", image_path)
        cv2.imwrite(image_path, img)

    h, w = img.shape[:2]
    if h < w:
        img = cv2.transpose(img)
        img = cv2.flip(img, flipCode=1)

    output = flow.process({'image': img})
    centers = []
    for i in range(len(output['tags'])):
        x1, x2, x3, x4, y1, y2, y3, y4 = np.int64(output['coordinates'][i])
        c = np.array([
                [x1, y1],
                [x2, y2],
                [x3, y3],
                [x4, y4],
            ]
        )
        M = cv2.moments(c)
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        center = [cX, cY]
        centers.append(center)

    centers = np.array(centers)
    for detection in y_true:
        true_center = np.array(detection['center'])
        if len(centers) == 0:
            detected_price = None
            detected_barcode = None
        else:
            distances = np.sum(np.abs(centers - true_center), axis = -1)
            closest_price_tag_idx = distances.argmin()
            # print(distances, closest_price_tag_idx, centers, true_center)
            detected_price = output['price'][closest_price_tag_idx]
            detected_price = float(detected_price) if detected_price is not None else None

            detected_barcode = output['barcode'][closest_price_tag_idx]
            detected_barcode = detected_barcode['value'] if detected_barcode is not None else None

        true_price = float(detection['price'])
        true_barcode = detection['barcode']

        mask = cv2.cvtColor(output['mask'], cv2.COLOR_GRAY2RGB)
        image = cv2.cvtColor(cv2.resize(img, dsize = mask.shape[::-1][1:]), cv2.COLOR_BGR2RGB)

        # log(output, log_path = 'logs/' + row['HITId'])

        if true_price == detected_price:
            price_hits += 1
        else:
            bad_prices['id'].append(row['HITId'])
            bad_prices['true'].append(true_price)
            bad_prices['detected'].append(detected_price)
            bad_prices['url'].append(row['Input.image_url'])

            print("bad prediction at ", row['HITId'])
            log(output, log_path = 'logs/' + row['HITId'])

        if true_barcode == detected_barcode:
            barcode_hits += 1
        else:
            bad_barcodes['id'].append(row['HITId'])
            bad_barcodes['true'].append(true_barcode)
            bad_barcodes['detected'].append(detected_barcode)
            bad_barcodes['url'].append(row['Input.image_url'])

        if true_barcode == detected_barcode and true_price == detected_price:
            both_hits += 1

pd.DataFrame.from_dict(bad_prices).to_csv('bad_prices.csv', index = False)
pd.DataFrame.from_dict(bad_barcodes).to_csv('bad_barcodes.csv', index = False)

with open('stats.txt', 'a') as f:
    f.write(f'correct_prices = {price_hits}\ncorrect_barcode = {barcode_hits}\nall_correct = {both_hits}\nsize = {len(data.index)}')
