import cv2
import numpy as np
from pyzbar.pyzbar import decode
from sklearn.cluster import DBSCAN
from scipy.signal import convolve2d
from workflow.workflow import ProcessorMixin


class BarcodeDetector(ProcessorMixin):
    def __init__(self, *args, **kwargs):
        pass

    def process(self, data):
        data['barcode'] = []
        data['tesseract_barcode'] = []
        data['cleaned_tags'] = []

        for idx, tag in enumerate(data['tags']):

            barcodes, enhanced = self.zbar_barcode(tag)
            data['barcode'].append(barcodes)
            data['cleaned_tags'].append(enhanced)

        return data

    def enhance_image(self, image, block_size=99, C=12):
        op_kernel = np.ones((3, 3))

        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        image = cv2.adaptiveThreshold(
            image,
            255,
            cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
            cv2.THRESH_BINARY,
            block_size,
            C
        )

        kernel = np.array([
            [0, -1, 0],
            [-1, 5, -1],
            [0, -1, 0]
        ])
        image = cv2.filter2D(image, -1, kernel)
        image = np.pad(image, (50, 50), mode="constant", constant_values=(255))

        return image

    def zbar_barcode(self, tag):
        # tag = cv2.resize(tag, dsize=None, fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC),
        enhanced = self.enhance_image(tag)
        barcodes = decode(enhanced)

        if not barcodes:
            return None, enhanced

        rect = barcodes[0].rect
        x, y, w, h = rect.left, rect.top, rect.width, rect.height

        return {
            'value': barcodes[0].data.decode('utf-8'),
            'coordinates': [x, y, w, h]
        }, enhanced
