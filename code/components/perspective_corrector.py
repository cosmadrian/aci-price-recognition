from workflow.workflow import ProcessorMixin
import math
import numpy as np
import cv2

class PerspectiveCorrector(ProcessorMixin):
    def __init__(self, *args, **kwargs):
        self.aspect_ratio = 1.5

    def process(self, _input):
        original_image = _input['image']
        mask = _input['mask']

        h, w = original_image.shape[0:2]
        hm, wm = mask.shape[0:2]

        fx = w / wm
        fy = h / hm

        _input['tags'], _input['coordinates'] = self.perspective_warp(original_image, mask, fx=fx, fy=fy)

        return _input

    def compute_warp_points(self, width, height):
        destination_points = np.array([
            [1, 0],
            [0, 0],
            [0, 1],
            [1, 1],
        ]).astype(np.float64)

        destination_points[:, 0] *= width
        destination_points[:, 1] *= height

        return destination_points.astype(np.int32)

    def get_points(self, contour):
        rect = cv2.minAreaRect(contour)
        box = cv2.boxPoints(rect)
        contour_points = np.squeeze(contour)

        final_points = np.zeros((4, 2), np.float32)
        for i, rect_point in enumerate(box):
            dist_l2 = np.sum((contour_points - rect_point)**2, axis=1)
            final_points[i] = contour_points[np.argmin(dist_l2)]

        return np.uint32(final_points)


    def perspective_warp(self, image, mask, fx=1.0, fy=1.0):
        output = cv2.findContours(mask, cv2.RETR_EXTERNAL, 2)

        if len(output) == 3:
            _, contours, hierarchy = output
        else:
            contours, hierarchy = output

        crops = []
        coordinates = []

        for c in contours:
            x1, y1, x2, y2, x3, y3, x4, y4 = self.get_points(c).ravel()

            x1, x2, x3, x4 = np.uint32(fx * np.array([x1, x2, x3, x4]))
            y1, y2, y3, y4 = np.uint32(fy * np.array([y1, y2, y3, y4]))

            max_y = np.max([y1, y2, y3, y4])
            min_y = np.min([y1, y2, y3, y4])

            max_height = max_y - min_y
            max_width = np.int32(max_height * self.aspect_ratio)

            destination_points = self.compute_warp_points(width=max_width, height=max_height)

            polygon = np.array([
                    [x4, y4],
                    [x3, y3],
                    [x2, y2],
                    [x1, y1],
                ]).tolist()

            mlat = sum(x[0] for x in polygon) / len(polygon)
            mlng = sum(x[1] for x in polygon) / len(polygon)

            def algo(x):
                return (math.atan2(x[0] - mlat, x[1] - mlng) - np.pi / 2) % (2*np.pi)

            polygon.sort(key=algo)
            polygon = np.array(polygon).astype(np.int32)

            H, _ = cv2.findHomography(polygon, np.squeeze(destination_points))
            dst = cv2.warpPerspective(image, H, dsize=(max_width, max_height))

            crops.append(dst)
            coordinates.append([x1, x2, x3, x4, y1, y2, y3, y4])

        return crops, coordinates
