import cv2
from workflow.workflow import ProcessorMixin
import numpy as np
import tensorflow.keras as keras
import sys
import os
from training.utils import ce_dl_loss
from keras_radam import RAdam

class LabelDetector(ProcessorMixin):
    MODEL = None

    def __init__(self, model_file=None, *args, **kwargs):
        if model_file is None:
            model_file = os.path.dirname(os.path.abspath(__file__)) + '/models/label_detector.hdf5'

        if LabelDetector.MODEL is None:
            LabelDetector.MODEL = keras.models.load_model(model_file, custom_objects={"RAdam": RAdam, 'ce_dl_loss': ce_dl_loss})

        self.MULTIPLIER = 6
        self.INPUT_WIDTH = 48 * self.MULTIPLIER
        self.INPUT_HEIGHT = 48 * self.MULTIPLIER * 2

    def process(self, _input):
        input_image = _input['image']
        input_image = cv2.resize(input_image, dsize=(self.INPUT_WIDTH, self.INPUT_HEIGHT), interpolation=cv2.INTER_AREA)
        input_image = input_image / 255.

        mask = LabelDetector.MODEL.predict(np.array([input_image]))[0]

        _input['mask_prediction'] = mask.copy()
        mask = self.post_process(mask)
        _input['mask'] = mask

        return _input

    def post_process(self, result):
        kernel = np.ones((5, 5))

        result[result < 0.5] = 0
        result[result >= 0.5] = 255

        result = cv2.morphologyEx(result, cv2.MORPH_OPEN, kernel, iterations = 2)
        result = cv2.morphologyEx(result, cv2.MORPH_CLOSE, kernel, iterations = 2)
        result = cv2.dilate(result, kernel, iterations = 1)

        return result.astype(np.uint8)

