from workflow.workflow import ProcessorMixin
import math
import numpy as np
import cv2

class PerspectiveCorrectorEnhanced(ProcessorMixin):
    def __init__(self, *args, **kwargs):
        self.aspect_ratio = 1.5 # TODO dependent on type of tag

    def process(self, _input):
        original_image = _input['image']
        mask = _input['mask']

        h, w = original_image.shape[0:2]
        hm, wm = mask.shape[0:2]

        fx = w / wm
        fy = h / hm

        _input['tags'], _input['coordinates'] = self.perspective_warp(original_image, mask, fx=fx, fy=fy)

        return _input

    def get_points(self, contour):
        rect = cv2.minAreaRect(contour)
        box = cv2.boxPoints(rect)
        contour_points = np.squeeze(contour)

        final_points = np.zeros((4, 2), np.float32)
        for i, rect_point in enumerate(box):
            dist_l2 = np.sum((contour_points - rect_point)**2, axis=1)
            final_points[i] = contour_points[np.argmin(dist_l2)]

        return np.uint32(final_points)

    def compute_warp_points(self, width, height):
        destination_points = np.array([
            [1, 0],
            [0, 0],
            [0, 1],
            [1, 1],
        ]).astype(np.float64)

        destination_points[:, 0] *= width
        destination_points[:, 1] *= height

        return destination_points.astype(np.int32)

    """
    A more interesting heuristic for rectangle polygon detections
    Contains checks for "bitten" masks, triangles and angles

    Inputs:
       - c: contour
       - arclength_eps: arclength epsilon for openCV filtering of really tiny segments
       - unfit_replace_threshold: multiplier of standard deviations which an L2 distance must 
                                  be above for its point to be located to the box prior
       - unfit_angle_threshold: +-degrees around 180 for which a rectangle corner is ignored
    """
    def contour_polyfind( self, c, arclength_eps = 0.01, unfit_replace_threshold = 5, unfit_angle_threshold = 5 ):
        # approximate polygon
        epsilon = arclength_eps * cv2.arcLength( c, True )
        poly_approx = cv2.approxPolyDP( c, epsilon, True )
        
        # convexify
        poly_convex = cv2.convexHull( poly_approx )
        
        # rotated rectangle prior
        poly_prior = cv2.minAreaRect( c )
        poly_prior = cv2.boxPoints( poly_prior )
        poly_prior = np.int0( poly_prior )
        
        # fitness to convex poly coordinates
        poly_fitness = []
        poly_selected_ind = []
        for p_prior in poly_prior: 
            distances = np.sqrt( np.sum( np.power( poly_convex - p_prior, 2 ), axis = 2 ) + 1e-9 )
            poly_fitness.append( np.min( distances ) )
            poly_selected_ind.append( np.argmin( distances ) )
        poly_fitness = np.array( poly_fitness )
        poly_selected_ind = np.array( poly_selected_ind )
        
        # double selection of the same contour point
        poly_selected_set = set( poly_selected_ind )
        if( len( poly_selected_set ) != len( poly_selected_ind ) ):
            if( len( poly_convex ) >= 4 ):
                mask = np.zeros( ( len( poly_convex ), ) )
                mask[ list( poly_selected_set ) ] = 1
                mask = np.logical_not( mask )
                double_ind = np.where( mask )[ 0 ][ 0 ]
                
                # block other points
                p_reduced = np.copy( poly_convex )
                p_reduced[ np.logical_not( mask ) ] = np.array( [ 1e6, 1e6 ] )
                
                # set point again
                distances = np.sqrt( np.sum( np.power( p_reduced - poly_prior[ double_ind ], 2 ), axis = 2 ) + 1e-9 )
                poly_fitness[ double_ind ] = np.min( distances )
                poly_selected_ind[ double_ind ] = np.argmin( distances )
        
        # select fit points
        poly = poly_convex[ poly_selected_ind ]

        # excess points
        if( len( poly_convex ) > 4 ):
            # check distances
            d_m = np.mean( poly_fitness )
            d_s = np.std( poly_fitness )
            d_fit = ( np.clip( poly_fitness, d_m - d_s, d_m + d_s ) == poly_fitness )
            
            # check least fitting point
            unfitness = np.abs( np.max( poly_fitness ) - np.mean( poly_fitness[ d_fit ] ) ) / ( np.std( poly_fitness[ d_fit ] ) + 1e-9 )
            # replace point with enclosing rectangle point if unfit
            if( unfitness > unfit_replace_threshold ):
                pmax = np.argmax( poly_fitness )
                poly[ pmax ] = poly_prior[ pmax ] 

        # triangle or something else, use prior box
        if( len( poly_convex ) < 4 ): 
            poly = np.array( [ poly_prior ] ).reshape( ( 4, 1, 2 ) )

        # angle checks
        poly = cv2.convexHull( poly )
        angles = [ np.arctan2( *list( ( poly[ i - 1 ][ 0 ] - poly[ i ][ 0 ] ) + 1e-9 )) for i in range( 4 ) ]
        angles = np.rad2deg( np.array( angles ) )
        pangles = np.abs( np.array( [ ( angles[ i - 1 ] - angles[ i ] ) % 180 for i in range( 4 ) ] ) - 180 )
        
        if( np.sum( pangles < unfit_angle_threshold ) ):
            poly = np.array( [ getPoints( c ) ] ).reshape( 4, 1, 2 ).astype( np.int )

        return np.clip( poly, 0, None )
        

    def perspective_warp(self, image, mask, fx=1.0, fy=1.0):
        output = cv2.findContours(mask, cv2.RETR_EXTERNAL, 2)

        if len(output) == 3:
            _, contours, hierarchy = output
        else:
            contours, hierarchy = output

        crops = []
        coordinates = []

        for c in contours:
            x1, y1, x2, y2, x3, y3, x4, y4 = self.contour_polyfind(c).ravel()

            x1, x2, x3, x4 = np.uint32(fx * np.array([x1, x2, x3, x4]))
            y1, y2, y3, y4 = np.uint32(fy * np.array([y1, y2, y3, y4]))

            max_y = np.max([y1, y2, y3, y4])
            min_y = np.min([y1, y2, y3, y4])

            max_height = max_y - min_y
            max_width = np.int32(max_height * self.aspect_ratio)

            destination_points = self.compute_warp_points(width=max_width, height=max_height)

            polygon = np.array([
                    [x4, y4],
                    [x3, y3],
                    [x2, y2],
                    [x1, y1],
                ]).tolist()

            mlat = sum(x[0] for x in polygon) / len(polygon)
            mlng = sum(x[1] for x in polygon) / len(polygon)

            def algo(x):
                return (math.atan2(x[0] - mlat, x[1] - mlng) - np.pi / 2) % (2*np.pi)

            polygon.sort(key=algo)
            polygon = np.array(polygon).astype(np.int32)

            H, _ = cv2.findHomography(polygon, np.squeeze(destination_points))
            dst = cv2.warpPerspective(image, H, dsize=(max_width, max_height))

            crops.append(dst)
            coordinates.append([x1, x2, x3, x4, y1, y2, y3, y4])

        return crops, coordinates
