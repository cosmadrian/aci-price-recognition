from workflow.workflow import ProcessorMixin
import numpy as np
import cv2
import os
from training.utils import ce_dl_loss
import tensorflow as tf
import tensorflow.keras as keras
from keras_radam import RAdam
import tensorflow.keras.backend as K

def focal_loss(gamma=2., alpha=.25):
    def focal_loss_fixed(y_true, y_pred):
        pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
        pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
        return -K.mean(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1)) - K.mean((1 - alpha) * K.pow(pt_0, gamma) * K.log(1. - pt_0))
    return focal_loss_fixed

class PriceDetector(ProcessorMixin):
    MODEL = None
    SEGMENTATION_MODEL = None
    CLASSES = '01234567890leio'

    WIDTH = 512
    HEIGHT = 256

    def __init__(self, model_file=None, price_segmentation_file=None, *args, **kwargs):
        if model_file is None:
            model_file = os.path.dirname(os.path.abspath(__file__)) + '/models/digit_ocr.hdf5'

        if price_segmentation_file is None:
            price_segmentation_file = os.path.dirname(os.path.abspath(__file__)) + '/models/price_segmentation.hdf5'

        if PriceDetector.MODEL is None:
            PriceDetector.MODEL = keras.models.load_model(
                model_file,
                custom_objects={'RAdam': RAdam, 'focal_loss_fixed': focal_loss()}
            )

        if PriceDetector.SEGMENTATION_MODEL is None:
            PriceDetector.SEGMENTATION_MODEL = keras.models.load_model(
                price_segmentation_file,
                custom_objects={'RAdam': RAdam, 'ce_dl_loss': ce_dl_loss}
            )

    def process(self, _input):
        _input['price'] = []
        _input['ocr_results'] = []
        _input['ocr_processed'] = []

        for tag in _input['tags']:
            image = cv2.resize(tag, dsize=(PriceDetector.WIDTH, PriceDetector.HEIGHT), interpolation=cv2.INTER_AREA)
            viz = image.copy()

            rects, processed = self._bounding_boxes(image)
            rects = self._filter_intersecting(rects)
            rects = self._adjust_boxes(rects, processed)
            digits = self._get_digits(image, rects)

            if not len(digits):
                _input['price'] += [None]
                _input['ocr_results'] += [[(None, None)]]
                _input['ocr_processed'] += [None]
                continue

            ocr_results = [
                (i, PriceDetector.CLASSES[idx])
                for i, idx in enumerate(np.argmax(PriceDetector.MODEL.predict(np.array(digits)), axis=-1))
            ]

            final_price = 0.0

            for (i, r) in sorted(ocr_results, key=lambda x: rects[x[0]]):
                if r in ['l', 'e', 'i', 'o']:
                    continue
                final_price = 10 * final_price + int(r)

            final_price = final_price / 100
            _input['price'] += [final_price]
            _input['ocr_results'] += [[(rects[i], r) for i, r in ocr_results]]
            _input['ocr_processed'].append(processed)

        return _input

    def _adjust_boxes(self, rects, result):
        if len(rects) < 2:
            return rects

        height_ratios = [rect[-1] / result.shape[0] for rect in rects]
        sorted_idx = np.argsort(height_ratios)
        split_idx = sorted_idx[np.argmax(np.diff([height_ratios[i] for i in sorted_idx]))]

        large_digits = [rects[i] for i in sorted_idx[split_idx + 1:]]
        small_digits = [rects[i] for i in sorted_idx[:split_idx + 1]]

        if not len(large_digits):
            return rects

        largest_x = np.max([rect[0] + rect[-2] / 5 for rect in large_digits])
        min_y_largest_digit = np.max([rect[1] for rect in large_digits])

        min_height_largest_digit = np.min([rect[-1] for rect in large_digits])

        if not len(small_digits):
            return rects

        small_digits = sorted(small_digits, key = lambda x: x[0])[:2]

        min_y = np.min([rect[1] for rect in small_digits])
        max_height = min_height_largest_digit + min_y_largest_digit - min_y

        if max_height <= 0:
            return rects

        final_digits = []
        corrected_small_digits = []
        for digit in small_digits:
            x, y, w, h = digit
            y = min_y
            h = max_height
            corrected_small_digits.append([x, y, w, h])

        if len(corrected_small_digits) == 2:
            if corrected_small_digits[1][0] < corrected_small_digits[0][0] + corrected_small_digits[0][2]:
                corrected_small_digits[1][2] = corrected_small_digits[1][2] - (corrected_small_digits[0][0] + corrected_small_digits[0][2] - corrected_small_digits[1][0])
                corrected_small_digits[1][0] = corrected_small_digits[0][0] + corrected_small_digits[0][2]

        final_digits.extend(corrected_small_digits)
        final_digits.extend(large_digits)

        return final_digits


    def _get_digits(self, image, rects):
        digits = []
        for r in rects:
            x, y, w, h = r

            d = image[y:y+h, x:x+w]
            d = cv2.resize(d, dsize=(32, 52))
            d = cv2.cvtColor(d, cv2.COLOR_RGB2GRAY)
            d = np.expand_dims(d, axis=-1)
            d = d / 255.

            digits.append(d)

        return digits

    def _bounding_boxes(self, image):
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        mask = np.uint8(PriceDetector.SEGMENTATION_MODEL.predict(np.expand_dims(image, axis=0) / 255.)[0] * 255.)

        _, mask = cv2.threshold(mask, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
        result = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
        result = np.pad(result, ((0, 1), (0, 1)), 'constant', constant_values = (255.))

        output = cv2.findContours(result, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        if len(output) == 3:
            _, contours, _ = output
        else:
            contours, _ = output

        rects = []
        for c in contours:
            x, y, w, h = cv2.boundingRect(c)
            if w * h > result.shape[0] * result.shape[1] / 4:
                continue

            if w * h <= 1000:
                continue

            if (y + h / 2) < result.shape[0] / 2.0:
                continue

            if x < result.shape[1] / 5:
                continue

            if h < w:
                continue

            rects.append([x, y, w, h])

        result = result[:-1, :-1]
        return rects, result

    def _intersection(self, a, b):
      x = max(a[0], b[0])
      y = max(a[1], b[1])
      w = min(a[0] + a[2], b[0] + b[2]) - x
      h = min(a[1] + a[3], b[1] + b[3]) - y

      if w <= 0 or h <= 0:
        return False

      return True

    def _filter_intersecting(self, rects):
        to_remove = []
        for i in range(0, len(rects)):
            x1, _, w1, _ = rects[i]

            for j in range(0, len(rects)):
                x2, _, w2, _, = rects[j]
                if i == j:
                    continue

                if w2 > w1:
                    continue

                if x1 < x2 and x1 + w1 > x2 + w2:
                    to_remove.append(j)

        return [rects[i] for i in range(len(rects)) if i not in to_remove]


    def _filter_intersecting_old(self, rects):
        final_rects = []
        for i in range(0, len(rects)):
            intersected = False
            x1, y1, w1, h1 = rects[i]
            for j in range(0, len(rects)):
                if i == j:
                    continue

                x2, y2, w2, h2 = rects[j]
                intersected = self._intersection(rects[i], rects[j])
                if intersected:
                    break

            if not intersected:
                final_rects.append(rects[i])
            else:
                if w1 * h1 > w2 * h2:
                    if rects[i] not in final_rects:
                        final_rects.append(rects[i])
                else:
                    if rects[j] not in final_rects:
                        final_rects.append(rects[j])

        return final_rects
