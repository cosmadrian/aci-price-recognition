import cv2
import json
from components import LabelDetector, PerspectiveCorrector
import numpy as np
import tensorflow as tf
from workflow import Workflow
import os
import base64
import matplotlib.pyplot as plt

from training.utils import aug_appearance, aug_position

folder = './images/'

label_detector = LabelDetector(model_file = 'training/checkpoints/2020-11-19-16:14/weights.150-0.19.hdf5')
perspective_corrector = PerspectiveCorrector()

for img in os.listdir(folder):
    _input = dict()
    image = cv2.imread(folder  + img)
    _input['image'] = image
    _input = label_detector.process(_input)
    _input = perspective_corrector.process(_input)

    cv2.imshow('frame', cv2.resize(image, dsize = None, fx = 0.2, fy = 0.2))
    cv2.waitKey(0)

    contours = np.array(_input['coordinates'])
    rgb_mask = cv2.cvtColor(_input['mask'], cv2.COLOR_GRAY2RGB)

    cv2.imshow('frame', _input['mask_prediction'])
    cv2.waitKey(0)

    cv2.imshow('frame', rgb_mask)
    cv2.waitKey(0)
    rgb_mask = cv2.resize(rgb_mask, dsize = image.shape[:2][::-1])

    for x1, x2, x3, x4, y1, y2, y3, y4 in contours:
        c = np.int32((np.array([
            [x1, y1],
            [x2, y2],
            [x3, y3],
            [x4, y4]
        ])))
        rgb_mask = cv2.drawContours(rgb_mask, [c], -1, (0, 255, 0), 35)

    rgb_mask = cv2.resize(rgb_mask, dsize = None, fx = 0.2, fy = 0.2)
    cv2.imshow('frame', rgb_mask)
    cv2.waitKey(0)

    for tag in _input['tags']:
        cv2.imshow('frame', tag)
        cv2.waitKey(0)
