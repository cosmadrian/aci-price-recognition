import cv2
import numpy as np
import os
import sys
import keras
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.utils import class_weight
import keras.backend as K
import tensorflow as tf
from keras_radam import RAdam
from sklearn.utils.multiclass import unique_labels
import matplotlib.pyplot as plt
import imgaug.augmenters as iaa
from sklearn.metrics import classification_report

seq_appearance = iaa.Sequential([
    iaa.OneOf([
        iaa.OneOf([
            iaa.GaussianBlur(sigma=(0, 2.0)), # blur images with a sigma of 0 to 3.0
            iaa.AverageBlur(k=((5, 11), (1, 3)))
        ]),
        iaa.OneOf([
            iaa.Sharpen(alpha=(0, 0.5), lightness=(0.75, 1.5)), # sharpen images
            iaa.Emboss(alpha=(0, 0.5), strength=(0, 1.0)), # emboss images
        ]),
    ]),
    iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5), # improve or worsen the contrast
    iaa.OneOf([
            iaa.Add((-50, 50), per_channel=0.5),
            iaa.Multiply((0.75, 1.25), per_channel=0.5),
        ]),
    iaa.Grayscale(alpha=(0.0, 1.0)),
    iaa.Affine(
        shear=(-4, 4),
        mode='reflect',
    )
])

def focal_loss(gamma=2., alpha=.25):
    def focal_loss_fixed(y_true, y_pred):
        pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
        pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
        return -K.mean(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1)) - K.mean((1 - alpha) * K.pow(pt_0, gamma) * K.log(1. - pt_0))
    return focal_loss_fixed

classes = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '0',
    'l',
    'e',
    'i',
    'o'
]

def get_data():
    y = []
    x = []
    for _class in os.listdir('annotated_digits'):
        for file in os.listdir(f'annotated_digits/{_class}/'):
            y.append(classes.index(_class))
            x.append(cv2.imread(f'annotated_digits/{_class}/{file}'))

    x = np.array(x)
    y = np.array(y)
    x, y = shuffle(x, y)

    return x, y

class DigitSequence(keras.utils.Sequence):
    def __init__(self, x, y, batch_size, kind='test'):
        self.x = x
        self.y = y
        self.batch_size = batch_size
        self.kind = kind

    def __getitem__(self, idx):
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size:(idx + 1) * self.batch_size]

        if self.kind == 'train':
            batch_x = seq_appearance.augment_images(batch_x)

        batch_x = np.expand_dims(
                np.array(
                    [cv2.cvtColor(x, cv2.COLOR_RGB2GRAY) for x in batch_x]
                ), axis=-1)

        return batch_x / 255., keras.utils.to_categorical(batch_y, num_classes=len(classes))

    def __len__(self):
        return int(np.ceil(len(self.x) / float(self.batch_size)))


model = keras.models.Sequential([
    keras.layers.Conv2D(64, 3, padding='SAME', input_shape=(52, 32, 1)),
    keras.layers.BatchNormalization(),
    keras.layers.LeakyReLU(),
    keras.layers.SpatialDropout2D(0.2),
    keras.layers.MaxPool2D(2),

    keras.layers.Conv2D(32, 3, padding='SAME'),
    keras.layers.BatchNormalization(),
    keras.layers.LeakyReLU(),
    keras.layers.SpatialDropout2D(0.2),
    keras.layers.MaxPool2D(2),

    keras.layers.Conv2D(16, 3, padding='SAME'),
    keras.layers.BatchNormalization(),
    keras.layers.LeakyReLU(),
    keras.layers.Flatten(),

    keras.layers.Dropout(0.5),
    keras.layers.Dense(len(classes), activation='softmax'),
])

model.compile(loss=focal_loss(), optimizer=RAdam(), metrics=['accuracy'])
model.summary()

x, y = get_data()
x_train, x_test, y_train, y_test = train_test_split(x, y, stratify=y)


model.fit_generator(
    generator=DigitSequence(x_train, y_train, 16, kind='train'),
    epochs=100,
    callbacks=[
        keras.callbacks.ModelCheckpoint('checkpoints/weights.{epoch:02d}-{val_loss:.6f}.hdf5', save_best_only=True),
    ],
    class_weight=class_weight.compute_class_weight('balanced', np.unique(y_train), y_train),
    validation_data=DigitSequence(x_test, y_test, 16, kind='test')
)

y_pred = np.argmax(model.predict(DigitSequence(x_test, y_test, 100000)[0][0]), axis=-1)
print(classification_report(y_test, y_pred, labels=classes, digits=4))

def plot_confusion_matrix(y_true, y_pred, labels, cmap=plt.cm.Blues):
    title = 'Normalized confusion matrix'

    cm = confusion_matrix(y_true, y_pred)
    labels = [labels[i] for i in unique_labels(y_true, y_pred)]
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           xticklabels=labels, yticklabels=labels,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    fmt = '.2f'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    fig.set_size_inches(7, 7)
    return ax

np.set_printoptions(precision=2)
plot_confusion_matrix(y_test, y_pred, labels=classes)
plt.savefig('confusion.png')
plt.show()

print("!!! TRAIN !!! ")
y_pred = np.argmax(model.predict(DigitSequence(x_train, y_train, 100000)[0][0]), axis=-1)
print(classification_report(y_train, y_pred, labels=classes, digits=4))

