import cv2
import os
import numpy as np

folder = 'dataset/'

tags = sorted(os.listdir(folder))

t = 0.9
iteration = 0
for i, tag in enumerate(tags):
    img = cv2.imread(folder + tag)
    img = cv2.resize(img, dsize=(440, 220))

    result = img.copy()
    viz = img.copy()
    result = cv2.cvtColor(result, cv2.COLOR_RGB2GRAY)
    element = np.ones((3, 3))

    result = cv2.adaptiveThreshold(result, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 127, 25)
    result =  cv2.dilate(result, element, iterations=1)
    result =  cv2.erode(result, element, iterations=1)

    contours, _ = cv2.findContours(result, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    digits = []
    rects = []
    final_rects = []
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        if w * h == 96800:
            continue

        if w * h <= 300:
            continue

        rects.append([x, y, w, h])

    final_rects = rects

    for (x, y, w, h) in final_rects:
        d = img[y:y+h, x:x+w]
        cv2.imwrite(f'digits/{iteration}.jpg', cv2.resize(d, dsize=(32, 52)))
        iteration += 1
