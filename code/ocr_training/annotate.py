import os
import cv2
import sys

classes = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '0',
    'l',
    'e',
    'i',
    'o'
]

for _class in classes:
    os.makedirs(f'annotated_digits/{_class}', exist_ok=True)

files = sorted(os.listdir('digits'))

for i, image in enumerate(files):
    print(f"{i}/{len(files)}")

    d = cv2.imread('digits/' + image)
    while True:
        cv2.imshow("frame", d)
        key = cv2.waitKey(0)
        if key in list(map(ord, classes)):
            os.rename(f'digits/{image}', f'annotated_digits/{chr(key)}/{image}')
            break
        elif key == 27:
            exit()
        else:
            continue
