import sys
import os
import cv2
sys.path.append('..')

from workflow import Workflow
import components


flow = Workflow([
    components.LabelDetector(),
    components.PerspectiveCorrector(),
    components.BestLabelSelector(), # ?
], verbose=False)


images_folder = '../../images/'
output_folder = 'dataset'

for i, image in enumerate(sorted(os.listdir(images_folder))):
    print(i, len(os.listdir(images_folder)))
    path = images_folder + image
    img = cv2.imread(path)
    try:
        output = flow.process({'image': img})

        for tag in output['tags']:
            cv2.imwrite(f'{output_folder}/{i}.jpg', tag)
    except Exception as e:
        print("Error on file", path, e)
