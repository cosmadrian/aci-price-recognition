import cv2
import numpy as np
import os
import sys
sys.path.insert(1, '/home/cosmadrian/Desktop/ml-pipeline/')
from workflow import Workflow
import components
import glob

price_workflow = Workflow([
    components.PriceDetector(),
], verbose=False)

flow = Workflow([
    components.LabelDetector(),
    components.PerspectiveCorrector(),
    price_workflow,
], verbose=False)

os.makedirs('./data/', exist_ok=True)

data = glob.glob('/home/cosmadrian/Desktop/ml-pipeline/images/*.jpg')
data += glob.glob('/home/cosmadrian/Desktop/ml-pipeline/new_dataset/images/*.jpg')


for idx, path in enumerate(sorted(data)):
    print(f"Creating Dataset: {round(idx/len(data), 4) * 100}")
    image = cv2.imread(path)
    output = flow.process({'image': image})

    for i, tag in enumerate(output['tags']):
        if output['price'][i] is None:
            print("price is none")
            continue

        if output['price'][i] < 1:
            print("price < 1")
            continue

        _tag = cv2.resize(tag, dsize=(440, 220))
        _tag2 = np.ones(_tag.shape[:-1]) * 255

        for coords, r in output['ocr_results'][i]:
            if r in 'leio':
                continue

            if coords is not None:
                (x, y, w, h) = coords
            else:
                continue

            x = max(x - 10, 0)
            y = max(y - 10, 0)
            w = min(x + w + 10, _tag.shape[1]) - x
            h = max(y + h + 10, _tag.shape[0]) - y

            digit_patch = _tag[y:y+h, x:x+w]

            digit_patch = cv2.cvtColor(digit_patch, cv2.COLOR_BGR2GRAY)
            digit_patch = cv2.adaptiveThreshold(
                digit_patch,
                255,
                cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                cv2.THRESH_BINARY,
                99,
                12
            )
            _tag2[y:y+h, x:x+w] = digit_patch

        cleaned = cv2.cvtColor(_tag2.astype(np.uint8), cv2.COLOR_GRAY2RGB)

        cv2.imwrite(f'data/{idx}.raw.jpg', _tag)
        cv2.imwrite(f'data/{idx}.clean.jpg', _tag2)
        print("saved images")
