import tensorflow.keras as keras
import imgaug
import cv2
import numpy as np
import os
from sklearn.utils import shuffle
import imgaug.augmenters as iaa
from sklearn.model_selection import train_test_split
from datetime import datetime


import sys
sys.path.insert(1, '/home/cosmadrian/Desktop/Projects/c4vr-infrastructure/ml-pipeline/')
from training.utils import make_model, DecodeCallback, ce_dl_loss

def get_data(directory):
    images = []
    labels = []

    prefixes = list(set([name.split('.')[0] for name in os.listdir(directory)]))

    for prefix in prefixes:
        image_path = directory + prefix + '.raw.jpg'
        label_path = directory + prefix + '.clean.jpg'

        if not os.path.exists(image_path) or not os.path.exists(label_path):
            continue

        images.append(image_path)
        labels.append(label_path)

    return images, labels

images, labels = get_data('./data/')

WIDTH = 512
HEIGHT = 256

x_train, x_test, y_train, y_test = train_test_split(images, labels, train_size=0.9)

seq_position = iaa.Sequential([
    iaa.Fliplr(0.3),
    iaa.Flipud(0.3),
])
seq_appearance = iaa.Sequential([
    iaa.OneOf([
            iaa.GaussianBlur(sigma=(0, 2.0)), # blur images with a sigma of 0 to 3.0
            iaa.AverageBlur(k=((5, 11), (1, 3)))
        ]),
    iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5), # improve or worsen the contrast
    iaa.OneOf([
            iaa.Sharpen(alpha=(0, 0.5), lightness=(0.75, 1.5)), # sharpen images
            iaa.Emboss(alpha=(0, 0.5), strength=(0, 1.0)), # emboss images
        ]),
    iaa.OneOf([
            iaa.Add((-50, 50), per_channel=0.5),
            iaa.Multiply((0.75, 1.25), per_channel=0.5),
        ]),
    iaa.Grayscale(alpha=(0.0, 1.0))
])

class PriceSegmentationSequence(keras.utils.Sequence):
    def __init__(self, batch_size, kind='train', data=None, seq_appearance=None, seq_position=None, remove_blobs=False):
        self.batch_size = batch_size
        self.kind = kind
        self.dsize = (WIDTH, HEIGHT)

        self.images, self.labels = data
        self.seq_position = seq_position
        self.seq_appearance = seq_appearance
        self.remove_blobs = remove_blobs

        self.closing_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    def __len__(self):
        return len(self.images) // self.batch_size

    def __getitem__(self, idx):
        images = self.images[idx * self.batch_size: (idx + 1) * self.batch_size]
        labels = self.labels[idx * self.batch_size: (idx + 1) * self.batch_size]

        x_batch = []
        y_batch = []

        for i in range(len(images)):
            image = cv2.imread(images[i], 1)
            label = cv2.imread(labels[i], 0)

            if self.remove_blobs:
                mask = cv2.morphologyEx(label, cv2.MORPH_CLOSE, self.closing_kernel)
                label = cv2.morphologyEx(mask, cv2.MORPH_OPEN, self.closing_kernel)

            x_batch.append(cv2.resize(image, dsize=self.dsize, interpolation=cv2.INTER_AREA))
            y_batch.append(cv2.resize(label, dsize=self.dsize, interpolation=cv2.INTER_AREA))

        if self.kind == 'train':
            seq_det = self.seq_position.to_deterministic()
            x_batch = self.seq_appearance.augment_images(
                seq_det.augment_images(x_batch)
            )
            y_batch = seq_det.augment_images(y_batch)

        x_batch = np.array(x_batch).astype(np.float32)
        y_batch = np.array(y_batch).astype(np.float32)

        y_batch[y_batch < 127] = 0.
        y_batch[y_batch >= 127] = 1.

        y_batch = np.expand_dims(y_batch, axis=-1)

        return x_batch / 255., y_batch

    def on_epoch_end(self):
        self.images, self.labels = shuffle(self.images, self.labels)


train_data_generator = PriceSegmentationSequence(
    batch_size=16,
    kind='train',
    data=(x_train, y_train),
    seq_position=seq_position,
    seq_appearance=seq_appearance,
    remove_blobs=True
)
test_data_generator = PriceSegmentationSequence(
    batch_size=16,
    kind='test',
    data=(x_test, y_test),
    seq_position=seq_position,
    seq_appearance=seq_appearance
)

checkpoint_path = './checkpoints/' + datetime.now().strftime("%Y-%m-%d-%H:%M")
os.makedirs(checkpoint_path, exist_ok=True)

viz_path = './viz/' + datetime.now().strftime("%Y-%m-%d-%H:%M")
os.makedirs(viz_path, exist_ok=True)

model = make_model(input_shape=(HEIGHT, WIDTH, 3), multiplier=4)
model.compile(loss=ce_dl_loss, optimizer='adam')
model.summary()

model.fit_generator(
        train_data_generator,
        epochs=150,
        callbacks=[
            # DecodeCallback(every=100, test_generator=test_data_generator, train_generator=train_data_generator, viz_path=viz_path),
            # keras.callbacks.ModelCheckpoint(checkpoint_path + '/weights.{epoch:02d}-{val_loss:.5f}.hdf5')
            keras.callbacks.TensorBoard('./logs/')
        ],
        validation_data=test_data_generator
    )

