from training.utils import *
import cv2


images, labels = get_data(
    '/home/cosmadrian/Desktop/Projects/c4vr-infrastructure/ml-pipeline/RINF/',
    num_images = 20
)

train_data_generator = PriceTagSequence(batch_size=10, kind='train', data=(images, labels))


X, Y = train_data_generator[1]

for i, (x, y) in enumerate(zip(X, Y)):
    y = cv2.cvtColor(y, cv2.COLOR_GRAY2RGB)
    vis = np.hstack([x, y]) * 255.
    vis = np.uint8(vis)
    cv2.imwrite(f'augs/{i}.png', vis)
    cv2.imshow("frame", vis)
    cv2.waitKey(0)
