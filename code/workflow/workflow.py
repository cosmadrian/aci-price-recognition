
class ProcessorMixin(object):
	def __repr__(self):
		return self.__class__.__name__

	def process(self, _input):
		raise NotImplementedError("You have to implement the process method.")

class Workflow(ProcessorMixin):
	def __init__(self, processors, *args, **kwargs):
		self.verbose = False
		self.processors = processors
		if kwargs.get('verbose', False):
			self.verbose = True

	def add_processor(self, processor):
		self.processors.append(processor)

	def process(self, _input):
		for processor in self.processors:
			if self.verbose:
				print(f'{processor} started processing ...')

			_input = processor.process(_input)

			if self.verbose:
				print(f'{processor} ended processing.')

		return _input
