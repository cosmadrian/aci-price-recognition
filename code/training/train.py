import imgaug
import cv2
import numpy as np
import os
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from datetime import datetime
import tensorflow as tf
import tensorflow.keras as keras

from utils import *

images, labels = get_data('/home/cosmadrian/Desktop/Projects/c4vr-infrastructure/ml-pipeline/RINF/')
# images, labels = get_data('/home/cosmadrian/Desktop/new_dataset/json/')

x_train, x_test, y_train, y_test = train_test_split(images, labels, train_size=0.9)

train_data_generator = PriceTagSequence(batch_size=4, kind='train', data=(x_train, y_train))
test_data_generator = PriceTagSequence(batch_size=4, kind='test', data=(x_test, y_test))

checkpoint_path = './weak_fine_tuned_checkpoints/' + datetime.now().strftime("%Y-%m-%d-%H:%M")
os.makedirs(checkpoint_path, exist_ok=True)

viz_path = './viz/' + datetime.now().strftime("%Y-%m-%d-%H:%M")
os.makedirs(viz_path, exist_ok=True)

model = make_model(multiplier = 2)

exit()
# model = keras.models.load_model('weak_checkpoints/weak_weights.98-0.18.hdf5', custom_objects={'RAdam': RAdam, 'ce_dl_loss': ce_dl_loss})
model.compile(loss=ce_dl_loss, optimizer = tf.keras.optimizers.Adam(lr = 0.001))
model.summary()

model.fit_generator(
		train_data_generator,
		epochs=150,
		callbacks=[
            tf.keras.callbacks.TensorBoard(log_dir="tensorboard", update_freq="batch"),
			DecodeCallback(every=100, test_generator=test_data_generator, train_generator=train_data_generator, viz_path=viz_path),
			keras.callbacks.ModelCheckpoint(checkpoint_path + '/weights.{epoch:02d}-{val_loss:.2f}.hdf5')
		],
		validation_data=test_data_generator
	)

