import imgaug
import cv2
import numpy as np
import os
import json
import base64
import imgaug.augmenters as iaa
from sklearn.utils import shuffle
from datetime import datetime
import tensorflow as tf
import tensorflow.keras as keras

MULTIPLIER = 6
WIDTH = 48 * MULTIPLIER
HEIGHT = 48 * MULTIPLIER * 2

BATCH_SIZE = 16
CHECKPOINT_DIR = 'checkpoints/'

aug_position = iaa.Sequential([
    iaa.Fliplr(0.5), # horizontally flip 50% of the images
    iaa.Flipud(0.5), # horizontally flip 50% of the images
    iaa.CropAndPad(percent=(-0.25, 0))
])
aug_appearance = iaa.Sequential([
    iaa.OneOf([
            iaa.GaussianBlur(sigma=(0, 2.0)), # blur images with a sigma of 0 to 3.0
            iaa.AverageBlur(k=((5, 11), (1, 3)))
        ]),
    iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5), # improve or worsen the contrast
    iaa.OneOf([
            iaa.Sharpen(alpha=(0, 0.5), lightness=(0.75, 1.5)), # sharpen images
            iaa.Emboss(alpha=(0, 0.5), strength=(0, 1.0)), # emboss images
        ]),
    iaa.OneOf([
            iaa.Add((-50, 50), per_channel=0.5),
            iaa.Multiply((0.75, 1.25), per_channel=0.5),
        ]),
    iaa.Grayscale(alpha=(0.0, 1.0))
])

def ce_dl_loss(y_true, y_pred):
    def dice_loss(y_true, y_pred):
        numerator = 2 * tf.reduce_sum(y_true * y_pred, axis=(1,2,3))
        denominator = tf.reduce_sum(y_true + y_pred, axis=(1,2,3))

        return tf.reshape(1 - numerator / denominator, (-1, 1, 1))

    return keras.losses.binary_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred)

class PriceTagSequence(keras.utils.Sequence):
    def __init__(self, batch_size, kind='train', data=None, seq_appearance=None, seq_position=None):
        self.batch_size = batch_size
        self.kind = kind
        self.dsize = (WIDTH, HEIGHT)

        self.images, self.labels = data
        self.seq_position = seq_position
        self.seq_appearance = seq_appearance

        if seq_position is None:
            self.seq_position = aug_position

        if seq_appearance is None:
            self.seq_appearance = aug_appearance

    def __len__(self):
        return len(self.images) // self.batch_size

    def __getitem__(self, idx):
        images = self.images[idx * self.batch_size: (idx + 1) * self.batch_size]
        labels = self.labels[idx * self.batch_size: (idx + 1) * self.batch_size]

        x_batch = []
        y_batch = []

        for i in range(len(images)):
            img = cv2.imdecode(images[i], cv2.IMREAD_COLOR)

            y_label = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
            for label in labels[i]:
                coords = np.int32(label.reshape((-1, 1, 2)))
                y_label = cv2.fillPoly(y_label, np.array([coords]), (255))

            x_batch.append(cv2.resize(img, dsize=self.dsize, interpolation=cv2.INTER_AREA))
            y_batch.append(cv2.resize(y_label, dsize=self.dsize, interpolation=cv2.INTER_AREA))

        if self.kind == 'train':
            seq_det = self.seq_position.to_deterministic()
            x_batch = self.seq_appearance.augment_images(
                seq_det.augment_images(x_batch)
            )
            y_batch = seq_det.augment_images(y_batch)

        x_batch = np.array(x_batch).astype(np.float32)
        y_batch = np.array(y_batch).astype(np.float32)

        y_batch[y_batch < 127] = 0.
        y_batch[y_batch >= 127] = 1.

        y_batch = np.expand_dims(y_batch, axis=-1)

        return x_batch / 255., y_batch

    def on_epoch_end(self):
        self.images, self.labels = shuffle(self.images, self.labels)


def make_model(input_shape=(HEIGHT, WIDTH, 3), multiplier = 2):
    input = keras.layers.Input(shape=input_shape)
    x = keras.layers.Conv2D(32 // multiplier, 3, padding='SAME')(input)
    x_1 = keras.layers.BatchNormalization()(x)
    x_1 = keras.layers.PReLU()(x_1)
    x_1 = keras.layers.SpatialDropout2D(0.05)(x_1)

    x = keras.layers.MaxPool2D(2)(x)
    x = keras.layers.Conv2D(64 // multiplier, 3, padding='SAME')(x)
    x_2 = keras.layers.BatchNormalization()(x)
    x_2 = keras.layers.LeakyReLU()(x_2)
    x_2 = keras.layers.SpatialDropout2D(0.05)(x_2)

    x = keras.layers.MaxPool2D(2)(x)
    x = keras.layers.Conv2D(128 // multiplier, 3, padding='SAME')(x)
    x_3 = keras.layers.BatchNormalization()(x)
    x_3 = keras.layers.LeakyReLU()(x_3)
    x_3 = keras.layers.SpatialDropout2D(0.05)(x_3)

    x = keras.layers.MaxPool2D(2)(x)
    x = keras.layers.Conv2D(128 // multiplier, 3, padding='SAME')(x)
    x_4 = keras.layers.BatchNormalization()(x)
    x_4 = keras.layers.LeakyReLU()(x_4)
    x_4 = keras.layers.SpatialDropout2D(0.05)(x_4)

    x = keras.layers.MaxPool2D(2)(x)
    x = keras.layers.Conv2D(256 // multiplier, 3, padding='SAME')(x)
    x_5 = keras.layers.BatchNormalization()(x)
    x_5 = keras.layers.LeakyReLU()(x_5)
    x_5 = keras.layers.SpatialDropout2D(0.05)(x_5)

    x = keras.layers.UpSampling2D(2)(x_5)
    x = keras.layers.Concatenate(axis=-1)([x, x_4])
    x = keras.layers.Conv2D(256 // multiplier, 3, padding='SAME')(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.LeakyReLU()(x)
    x = keras.layers.SpatialDropout2D(0.05)(x)

    ################################################################
    x = keras.layers.Conv2D(256 // multiplier, 3, padding='SAME')(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.LeakyReLU()(x)
    x = keras.layers.SpatialDropout2D(0.05)(x)
    ###############################################################

    x = keras.layers.UpSampling2D(2)(x)
    x = keras.layers.Concatenate(axis=-1)([x, x_3])
    x = keras.layers.Conv2D(128 // multiplier, 3,  padding='SAME')(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.LeakyReLU()(x)
    x = keras.layers.SpatialDropout2D(0.05)(x)
    ################################################################
    x = keras.layers.Conv2D(128 // multiplier, 3,  padding='SAME')(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.LeakyReLU()(x)
    x = keras.layers.SpatialDropout2D(0.05)(x)
    ################################################################

    x = keras.layers.UpSampling2D(2)(x)
    x = keras.layers.Concatenate(axis=-1)([x, x_2])
    x = keras.layers.Conv2D(64 // multiplier, 3, padding='SAME')(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.LeakyReLU()(x)
    x = keras.layers.SpatialDropout2D(0.05)(x)

    ################################################################
    x = keras.layers.Conv2D(64 // multiplier, 3, padding='SAME')(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.LeakyReLU()(x)
    x = keras.layers.SpatialDropout2D(0.05)(x)
    ################################################################

    x = keras.layers.UpSampling2D(2)(x)
    x = keras.layers.Conv2D(64 // multiplier, 3, padding='SAME')(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.LeakyReLU()(x)
    x = keras.layers.SpatialDropout2D(0.05)(x)

    x = keras.layers.Conv2D(1, 3, activation='sigmoid', padding='SAME')(x)

    model = keras.models.Model(input, x)
    return model


def get_data(directory, num_images = None):
    data = os.listdir(directory)
    images = []
    labels = []

    for i, image_path in enumerate(data[:num_images]):
        print(i, len(data))
        with open(directory + image_path, 'rt') as f:
            image_data = json.load(f)

        imgb64 = image_data['imageData']
        img = np.fromstring(base64.b64decode(imgb64), np.uint8)
        # img = cv2.imdecode(img, cv2.IMREAD_COLOR)
        shapes = image_data['shapes']

        _labels = []
        for shape in shapes:
            points = np.array(shape['points'])
            _labels.append(points)
        labels.append(_labels)
        images.append(img)

    return images, labels


class DecodeCallback(keras.callbacks.Callback):
    def __init__(self, test_generator, train_generator, every=2, viz_path='viz/', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test_generator = test_generator
        self.train_generator = train_generator
        self.every = every
        self.iteration = 0
        self.viz_path = viz_path

    def on_batch_end(self, *args, **kwargs):
        self.iteration += 1

        if self.iteration % self.every == 0:
            images, labels = self.test_generator[np.random.randint(0, len(self.test_generator))]
            train_images, train_labels = self.train_generator[np.random.randint(0, len(self.train_generator))]
            preds = self.model.predict(images)
            train_preds = self.model.predict(train_images)

            rows = []
            for image, pred, true, train_image, train_pred, train_true in zip(images, preds, labels, train_images, train_preds, train_labels):
                true = cv2.cvtColor(np.squeeze(true), cv2.COLOR_GRAY2RGB)
                pred = cv2.cvtColor(np.squeeze(pred), cv2.COLOR_GRAY2RGB)

                train_true = cv2.cvtColor(np.squeeze(train_true), cv2.COLOR_GRAY2RGB)
                train_pred = cv2.cvtColor(np.squeeze(train_pred), cv2.COLOR_GRAY2RGB)

                rows.append(np.hstack((image, true, pred, train_image, train_true, train_pred)))

            out = np.vstack(rows)
            output = cv2.resize(out, dsize=None, fx=0.5, fy=0.5)
            output = np.uint8(output * 255)
            cv2.imwrite(self.viz_path +  f'/image.{self.iteration}.png', output)
