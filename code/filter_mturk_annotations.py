from PIL import Image
import base64
import cv2
import numpy as np
import pandas as pd

import requests
from io import StringIO, BytesIO

def hex_to_rgb(hex):
    hex = hex.lstrip('#')
    hlen = len(hex)
    return tuple(int( hex[i:i + hlen // 3], 16) for i in range(0, hlen, hlen // 3))

df = pd.read_csv('Batch_3904874_batch_results.csv')
alpha = 0.5

approval_df = df.copy()
approval_df['Approve'] = approval_df['Approve'].astype(str)
approval_df['Reject'] = approval_df['Reject'].astype(str)

def number_of_instances(row, annotation):
    instances = eval(row['Answer.annotatedResult.instances'])

    if len(instances) < 3:
        return True

    return False

def no_price_tag(row, annotation):
    instances = eval(row['Answer.annotatedResult.instances'])

    for instance in instances:
        if instance['label'] == 'price-tag':
            return False

    return True

def multiple_barcodes_in_price_tag(row, annotation):
    instances = eval(row['Answer.annotatedResult.instances'])

    price_tag_idxs = np.argwhere([instance['label'] == 'price-tag' for instance in instances])
    barcode_idxs = np.argwhere([instance['label'] == 'barcode' for instance in instances])

    for price_tag_idx in price_tag_idxs:
        number_intersecting = 0

        price_tag = np.zeros((*annotation.shape, 1))

        polygon_y, polygon_x = np.where(annotation == price_tag_idx + 1)
        points = np.hstack((polygon_x.reshape((-1, 1)), polygon_y.reshape((-1, 1))))
        points = points.reshape(-1, 1, 2)

        cv2.polylines(price_tag, [points], True, 1)

        for barcode_idx in barcode_idxs:
            barcode = np.zeros((*annotation.shape, 1))

            polygon_y, polygon_x = np.where(annotation == barcode_idx + 1)
            points = np.hstack((polygon_x.reshape((-1, 1)), polygon_y.reshape((-1, 1))))
            points = points.reshape(-1, 1, 2)
            cv2.polylines(barcode, [points], True, 1)

            if np.any(np.logical_and(price_tag, barcode)):
                number_intersecting += 1

            if number_intersecting == 2:
                return True

    return False

def multiple_or_missing_prices_in_price_tag(row, annotation):
    instances = eval(row['Answer.annotatedResult.instances'])

    price_tag_idxs = np.argwhere([instance['label'] == 'price-tag' for instance in instances])
    price_idxs = np.argwhere([instance['label'] == 'price' for instance in instances])

    for price_tag_idx in price_tag_idxs:
        number_intersecting = 0

        price_tag = np.zeros((*annotation.shape, 1))

        polygon_y, polygon_x = np.where(annotation == price_tag_idx + 1)
        points = np.hstack((polygon_x.reshape((-1, 1)), polygon_y.reshape((-1, 1))))
        points = points.reshape(-1, 1, 2)

        cv2.polylines(price_tag, [points], True, 1)

        for price_idx in price_idxs:
            price = np.zeros((*annotation.shape, 1))

            polygon_y, polygon_x = np.where(annotation == price_idx + 1)
            points = np.hstack((polygon_x.reshape((-1, 1)), polygon_y.reshape((-1, 1))))
            points = points.reshape(-1, 1, 2)
            cv2.polylines(price, [points], True, 1)

            if np.any(np.logical_and(price_tag, price)):
                number_intersecting += 1

            if number_intersecting == 2:
                return True

        if number_intersecting == 0:
            return True

    return False

rejected_idx = []
reasons = []

for i, row in df.iterrows():
    print(i, len(df.index))
    annotation = Image.open(BytesIO(base64.b64decode(row['Answer.annotatedResult.labeledImage.pngImageData'])))
    annotation = np.array(annotation)

    rejected = False
    for criterion in [
            no_price_tag,
            number_of_instances,
            multiple_barcodes_in_price_tag,
            multiple_or_missing_prices_in_price_tag,
        ]:

        if criterion(row, annotation):
            approval_df.at[i, 'Reject'] = criterion.__name__
            rejected = True
            break

    if not rejected:
        approval_df.at[i, 'Approve'] = 'x'

    print(approval_df.iloc[i][['Approve', 'Reject']])

approval_df.to_csv('approval.csv', index = False)
